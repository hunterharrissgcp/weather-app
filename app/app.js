
const geocode = require('./utils/geocode');
const forecast = require('./utils/forecast');

const city = process.argv[2];

if (!city) {
  console.log('Please provide a city');
} else {
  geocode(city, (error, {latitude, longitude, location} = {}) => {
    if (error) {
      return console.log('Error', error);
    }
    return forecast(latitude, longitude, (forecastError, forecastData) => {
      if (forecastError) {
        console.log('Error', forecastError);
      } else {
        console.log(location);
        console.log(forecastData);
      }
    });
  });
}
